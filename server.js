/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var forecast = require('./routes/forecast')
var http = require('http');
var path = require('path');
var cors = require('cors');
var app = express();
// Parser Validation
app.configure(function () {
    app.use(express.bodyParser());
});
// all environments
app.set('port', process.env.OPENSHIFT_NODEJS_PORT || 3024);
app.set('ip', process.env.OPENSHIFT_NODEJS_IP);
app.set('views', path.join(__dirname, 'views'));
app.use(cors());
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}
//Middelwares for toked validate
app.use(function (req, res) {
    // redirect 
    res.json(200, "you lost");
});
//Add all routes API
require('./router.js')(app);

app.listen(app.get('port'), app.get('ip'), function () {    
    console.log("[Server] Express server listening on port " + app.get('port') + "");
});
