var _ = require('underscore');

var _range = {
    precipMM: '0.1',
    cloudcover: 15,
    temp_C: 3,
    windspeedKmph: 5,
    humidity: 5
}
// forecast
exports.add = function (req, res, next) {
    var w = req.body;
    w.clothes = [];
    w.clothes = JSON.parse(w.mydress);
    w.mydress = undefined;
    w.mydress = undefined;
    var weather = new req.db.Weather(w);
    weather.save(function (err, u) {
        res.send(200);
    });
};

exports.getAll = function (req, res, next) {
    req.db.Weather.find({
        precipMM: {
            $gte: new Number(req.body.precipMM) - new Number(_range.precipMM),
            $lt: new Number(req.body.precipMM) + new Number(_range.precipMM)
        },
        cloudcover: {
            $gte: req.body.cloudcover - _range.cloudcover,
            $lt: req.body.cloudcover + _range.cloudcover
        },
        temp_C: {
            $gte: req.body.temp_C - _range.temp_C,
            $lt: req.body.temp_C + _range.temp_C
        },
        windspeedKmph: {
            $gte: req.body.windspeedKmph - _range.windspeedKmph,
            $lt: req.body.windspeedKmph + _range.windspeedKmph
        },
        humidity: {
            $gte: req.body.humidity - _range.humidity,
            $lt: req.body.humidity + _range.humidity
        }
    }).exec(function (err, u) {
        if (!err) {

            var _allClothes = [];
            for (var i = 0; i < u.length; i++) {
                _allClothes.push(u[i].clothes);
            }
            var data = {};
            data.hat = _.countBy(_allClothes, function (obj) {
                return obj.hat;
            });
            data.bust = _.countBy(_allClothes, function (obj) {
                return obj.bust;
            });
            data.legs = _.countBy(_allClothes, function (obj) {
                return obj.legs;
            });
            data.umbrella = _.countBy(_allClothes, function (obj) {
                return obj.umbrella;
            });
            data.jacket = _.countBy(_allClothes, function (obj) {
                return obj.jacket;
            });
            

            res.json(200, data);
        } else {
            res.json(500)
        }

    });
};

exports.getOne = function (req, res, next) {
   
    req.db.Weather.find({
        userId: req.params.userId,
        precipMM: {
            $gte: new Number(req.body.precipMM) - new Number(_range.precipMM),
            $lt: new Number(req.body.precipMM) + new Number(_range.precipMM)
        },
        cloudcover: {
            $gte: req.body.cloudcover - _range.cloudcover,
            $lt: req.body.cloudcover + _range.cloudcover
        },
        temp_C: {
            $gte: req.body.temp_C - _range.temp_C,
            $lt: req.body.temp_C + _range.temp_C
        },
        windspeedKmph: {
            $gte: req.body.windspeedKmph - _range.windspeedKmph,
            $lt: req.body.windspeedKmph + _range.windspeedKmph
        },
        humidity: {
            $gte: req.body.humidity - _range.humidity,
            $lt: req.body.humidity + _range.humidity
        }
    }).exec(function (err, u) {

        if (!err) {

            var _allClothes = [];
            for (var i = 0; i < u.length; i++) {
                console.log(u[i].clothes);
                _allClothes.push(u[i].clothes);
            }
            var data = {};
            data.hat = _.countBy(_allClothes, function (obj) {
                return obj.hat;
            });
            data.bust = _.countBy(_allClothes, function (obj) {
                return obj.bust;
            });
            data.legs = _.countBy(_allClothes, function (obj) {
                return obj.legs;
            });
            data.umbrella = _.countBy(_allClothes, function (obj) {
                return obj.umbrella;
            });
            data.jacket = _.countBy(_allClothes, function (obj) {
                return obj.jacket;
            });

            res.json(200, data);
        } else {
            res.json(500)
        }

    });

};