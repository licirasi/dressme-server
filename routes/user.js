/*
 * GET users listing.
 */
exports.list = function (req, res) {

     req.db.Weather.find({}).distinct("userId").exec(function (err, users) {
        if (err) return res.json(500)
        else if (!users) res.json(404)
        else res.json(200, users)
    });
};
