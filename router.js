var mongoose = require('mongoose'),
    _ = require('underscore'),
    models = require('./models'),
    routes = require('./routes');

var _apykey = "vivalafigadellamammadeglistoltileccesi01234567suca";

var _apykeyValidation = function (req, res, next) {
    console.log(req.body)
    if (req.body.apikey + "" == _apykey) return next();
    return res.send(403);
};

/*
    This file route the request and and the middelware
    all API are in routesCollections array
*/

// All Api 
var routesCollections = [

    {
        path: '/hello',
        httpMethod: 'GET',
        middleware: [routes.hello]
    },
    { // Only for serve pic
        path: '/api/1/users',
        httpMethod: 'GET',
        middleware: [db,
            function (req, res, next) {
                if (req.headers['x-dressme-apikey'] == "nP7WzkoCM3g876HL9qLSVkTBgmDl22KFJF73KcKzxwB8975626651JPsMYNHp3r95T2xttPWb7M9vdX4IkHvoKagc7TQR1SZ") return next();
                return res.json(401)
        },
            routes.user.list]
    },
 //    {
 //        path: '/',
 //        httpMethod: 'GET',
 //        middleware: [ /*routes.index*/ ]
 //    },

 //forecast

    { // Only for serve pic
        path: '/api/1/forecast',
        httpMethod: 'POST',
        middleware: [_apykeyValidation, db, routes.forecast.add]
    },

    { // Only for serve pic
        path: '/api/1/forecast/get',
        httpMethod: 'POST',
        middleware: [db, routes.forecast.getAll]
    },
    { // Only for serve pic
        path: '/api/1/forecast/get/:userId',
        httpMethod: 'POST',
        middleware: [db, routes.forecast.getOne]
    }
];

module.exports = function (app) {
    _.each(routesCollections, function (route) {
        var args = _.flatten([route.path, route.middleware]);

        switch (route.httpMethod.toUpperCase()) {
        case 'GET':
            app.get.apply(app, args);
            break;
        case 'POST':
            app.post.apply(app, args);
            break;
        case 'PUT':
            app.put.apply(app, args);
            break;
        case 'DELETE':
            app.delete.apply(app, args);
            break;
        default:
            //throw new Error('Invalid HTTP method specified for route ' + route.path);
            consle.log("Error");
            break;
        }
    });

};

// Database conneciton URL
var dbUrl = "mongodb://twentythree:VivaLamamma2013@ds035997.mongolab.com:35997/dressmeproduction";

var database = mongoose.createConnection(dbUrl);
database.on('error', function () {

    console.log("Connection error");
});
database.once('open', function () {
    console.log("Connection correct ");
});

// Database Middelware
function db(req, res, next) {
    req.db = {
        Weather: database.model('Weather', models.Weather, 'weathers')
    };
    return next();
}
