// User model for database
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

exports.Weather = new Schema({
    userId: {
        type: Number,
        required: true
    },
    weatherCode: Number,
    precipMM: Number,
    cloudcover: Number,
    pressure: Number,
    temp_C: Number,
    windspeedKmph: Number,
    humidity: Number,
    clothes: {
        hat: String,
        bust: String,
        legs: String,
        umbrella: String,
        jacket: String
    }
});